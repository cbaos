#ifndef TYPES_H_
#define TYPES_H_

#include <stddef.h> /* size_t and so on */

#ifndef u8
#define u8 unsigned char
#endif
#ifndef u16
#define u16 unsigned short
#endif
#ifndef u32
#define u32 unsigned int
#endif

#ifndef s8
#define s8 signed char
#endif
#ifndef s16
#define s16 signed short
#endif
#ifndef s32
#define s32 signed int
#endif

#endif
